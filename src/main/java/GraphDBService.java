import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

/**
 * Created with IntelliJ IDEA.
 * User: eidonfiloi
 * Date: 7/24/13
 * Time: 7:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class GraphDBService {

    private String path;

    private GraphDatabaseService graphDb;

    public GraphDBService(String path) {
        this.path = path;
        this.graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(this.path);
    }

    public GraphDatabaseService getGraphDb() {
        return graphDb;
    }
}
