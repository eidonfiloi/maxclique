# Cats & Dogs ... a MaxClique problem

Here you can find more information about my solution to the cats & dogs problem.

## Problem transformation

For every test one can create an undirected graph, such that the vertices are the voters and there is an edge between two voters (A and B) iff their votes are compatible with each other, i.e. **if( ! (A_voteFor == B_voteAgainst || A_voteAgainst == B_voteFor) )**. Then the problem is find a maximal complete subgraph in this graph, or aka the maximum clique problem. 

## The Algorithm ##

The maximum clique problem is a well-known **NP-hard problem**. Most exact algorithms apply some form of **branch-and-bound** approach. While branching systematically searches for all candidate solutions, bounding (also known as pruning) discards fruitless candidates based on a previously computed bound. A simple and effective branch-and-bound algorithm for the maximum clique problem is one by **Carraghan and Pardalos** [*R. Carraghan and P. Pardalos, An exact algorithm for the maximum clique problem, Oper. Res. Lett. 9 (1990), pp.375–382.*]. There are many variations that make this base algorithm more effective by fine tuning the pruning part. I use a relatively **new variant** developed by Bharath Pattabiraman, Md. Mostofa Ali Patwary, Assefaw H. Gebremedhin†, Wei-keng Liao and Alok Choudhary in 2012 [*Bharath Pattabiraman et al.,Fast Algorithms for the Maximum Clique Problem on Massive Sparse Graphs,Optimization Methods and Software Vol. 00 No. 00 November 2012 pp.1–14*]. Complexity: O(v · ∆ · ∆), where v is the number of voters/vertices and ∆ is the max degree in the graph.

## Implementation ##

I created a Java Maven project to solve the task. I use Neo4j for graph representation. There is a packaged jar with dependencies, **/target/MaxClique-1.0-jar-with-dependencies.jar**. It should get three arguments, the path to the test file, a path for storing the neo4j db's and an output path.  