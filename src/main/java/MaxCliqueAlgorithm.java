import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.kernel.EmbeddedGraphDatabase;
import org.neo4j.kernel.EmbeddedReadOnlyGraphDatabase;

import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA.
 * User: eidonfiloi
 * Date: 7/24/13
 * Time: 9:05 AM
 * To change this template use File | Settings | File Templates.
 */
public class MaxCliqueAlgorithm {

    private GraphDatabaseService graph;

    private int max;

    private int graphSize;

    public MaxCliqueAlgorithm(GraphDatabaseService graph, int graphSize) {
        this.graph = graph;
        this.graphSize = graphSize;
        this.max = 0;
        mainAlgorithm();
        System.out.println("max " + max);
    }

    private void mainAlgorithm() {

        Iterable<Node> allNodes = this.graph.getAllNodes();
        Iterator<Node> iterator = allNodes.iterator();
        while(iterator.hasNext()) {
            Node currentNode = iterator.next();
            int degree = getDegree(currentNode);
            if(degree >= this.max) {
                PriorityQueue<Node> qU = new PriorityQueue<Node>(this.graphSize,new Comparator<Node>() {


                    @Override
                    public int compare(Node o1, Node o2) {
                        Integer d1 = getDegree(o1);
                        Integer d2 = getDegree(o2);
                        return -d1.compareTo(d2);  //To change body of implemented methods use File | Settings | File Templates.
                    }
                });
                Iterable<Relationship> edges = currentNode.getRelationships(RelType.LINK);
                Iterator<Relationship> edgeIterator = edges.iterator();
                while(edgeIterator.hasNext()) {
                    Relationship currentEdge = edgeIterator.next();
                    Node endNode = currentEdge.getEndNode();
                    if((getDegree(endNode)) >= this.max) {
                        qU.add(endNode);
                    }
                }
                this.cliqueAlgorithm(qU, 1);
            }
        }
    }

    private int getDegree(Node n) {
        int degree = 0;

        Iterable<Relationship> links = n.getRelationships(RelType.LINK);
        Iterator<Relationship> it = links.iterator();
        while(it.hasNext()) {
            Relationship rel = it.next();
            degree++;
        }

        return degree;
    }

    private void cliqueAlgorithm(PriorityQueue<Node> qU,int size) {

        if(qU.isEmpty()) {
            if(size > this.max) {
                this.max = size;
            }
            return;
        }
        Node nextNode = qU.poll();
        /**
         *          N′(u) := {w|w ∈ N(u) ∧ d(w) ≥ max} ◃ Pruning 5
         *
         *          CLIQUEHEU(G,U ∩N′(u),size+1)
         */
        PriorityQueue<Node> nextQ = new PriorityQueue<Node>(this.graphSize,new Comparator<Node>() {
            @Override
            public int compare(Node o1, Node o2) {
                Integer d1 = getDegree(o1);
                Integer d2 = getDegree(o2);
                return -d1.compareTo(d2);  //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        Iterable<Relationship> edges = nextNode.getRelationships(RelType.LINK);
        Iterator<Relationship> edgeIterator = edges.iterator();
        while(edgeIterator.hasNext()) {
            Relationship edge = edgeIterator.next();
            Node n = edge.getEndNode();
            Integer currentDegree = getDegree(n);
            if(currentDegree >= this.max) {
                nextQ.add(n);
            }
        }
        qU.retainAll(nextQ);
        cliqueAlgorithm(qU,size + 1);
    }

    public int getMax() {
        return max;
    }
}
