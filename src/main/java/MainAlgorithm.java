import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.kernel.EmbeddedGraphDatabase;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: eidonfiloi
 * Date: 7/24/13
 * Time: 8:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainAlgorithm {

    private static  String testPath;// = "/Users/eidonfiloi/CatsAndDogs/catsdogs.txt";

    private static String graphDbPath;// = "/Users/eidonfiloi/CatsAndDogs";

    private static  String outPath;// = "/Users/eidonfiloi/CatsAndDogs/result.txt";

    private static GraphDatabaseService graph;

    public static void main(String[] args) throws IOException {

        if(args.length != 3) {
           System.err.println("Please give three arguments: the test path, the graphDB path and the output path.");
        }
        testPath = args[0];
        graphDbPath = args[1];
        outPath = args[2];

        PrintWriter writer = new PrintWriter(new FileWriter(outPath));
        GraphCreator graphCreator = new GraphCreator(testPath,graphDbPath);

        HashMap<String,Integer> graphPathMap = graphCreator.getGraphPathMap();
        ArrayList<String> graphPathList = graphCreator.getGraphPathList();

        for(String path : graphPathList) {

            graph = new GraphDatabaseFactory().newEmbeddedDatabase(path);
            registerShutdownHook(graph);
            MaxCliqueAlgorithm algo = new MaxCliqueAlgorithm(graph,graphPathMap.get(path));
            int max = algo.getMax();
            writer.println(max);
            graph.shutdown();
        }

        writer.close();

    }

    private static void registerShutdownHook( final GraphDatabaseService graphDb )
    {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook( new Thread()
        {
            @Override
            public void run()
            {
                graphDb.shutdown();
            }
        } );
    }
}
