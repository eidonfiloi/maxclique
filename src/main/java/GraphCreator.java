
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: eidonfiloi
 * Date: 7/24/13
 * Time: 9:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class GraphCreator {

    private String inputPath;

    private String graphdbPath;

    private HashMap<String,Integer> graphPathMap;

    private ArrayList<String> graphPathList;

    private BufferedReader reader;

    private int numOfTests;

    public GraphCreator(String inputPath, String graphdbPath) throws IOException {
        this.inputPath = inputPath;
        this.graphdbPath = graphdbPath;
        this.graphPathMap = new HashMap<String,Integer>();
        this.graphPathList = new ArrayList<String>();
        preprocess();

    }

    private void preprocess() throws IOException {

        this.reader = new BufferedReader(new FileReader(inputPath));

        String line = reader.readLine();
        this.numOfTests = Integer.parseInt(line);

        line = reader.readLine();
        String[] lineStr = line.split(" ");
        int v = Integer.parseInt(lineStr[2]);
        int numOfLine = 0;
        int numOfGraphs = 1;
        ArrayList<NodeClass> ncList = new ArrayList<NodeClass>();


        String curDbPath = this.graphdbPath + "/graph" + numOfGraphs + ".db";
        GraphDBService service = new GraphDBService(curDbPath);
        this.graphPathMap.put(curDbPath,1);
        GraphDatabaseService graphDb = service.getGraphDb();
        registerShutdownHook(graphDb);
        while((line = reader.readLine()) != null) {
            numOfLine++;
            lineStr = line.split(" ");
            if(numOfLine <= v) {
                String voteFor = lineStr[0];
                String voteAgainst = lineStr[1];
                NodeClass nc = new NodeClass(voteFor,voteAgainst);
                ncList.add(nc);
            } else {
                createGraph(ncList,graphDb);
                System.out.println("Size " + ncList.size());
                this.graphPathList.add(curDbPath);
                this.graphPathMap.put(curDbPath,ncList.size());
                v = Integer.parseInt(lineStr[2]);
                numOfLine = 0;
                graphDb.shutdown();
                numOfGraphs++;
                ncList = new ArrayList<NodeClass>();
                curDbPath = this.graphdbPath + "/graph" + numOfGraphs + ".db";
                service = new GraphDBService(curDbPath);
                this.graphPathMap.put(curDbPath,1);
                graphDb = service.getGraphDb();
                registerShutdownHook(graphDb);
            }
        }

        createGraph(ncList,graphDb);
        System.out.println("Size " + ncList.size());
        this.graphPathList.add(curDbPath);
        this.graphPathMap.put(curDbPath,ncList.size());
        graphDb.shutdown();
        reader.close();
    }

    private void createGraph(ArrayList<NodeClass> ncList, GraphDatabaseService graphDb) {

        int len = ncList.size();
        HashMap<Integer,Node> nodes = new HashMap<Integer,Node>();
        Transaction tx = graphDb.beginTx();
        try
        {
            for(NodeClass nc : ncList) {
                Node node = graphDb.createNode();
                Integer index = ncList.indexOf(nc);
                node.setProperty("title",index);
                node.setProperty("voteFor",nc.getVoteFor());
                node.setProperty("voteAgainst",nc.getVoteAgainst());
                nodes.put(index,node);
            }

            for(int i = 0; i < len - 1; i++) {
               NodeClass ncOne = ncList.get(i);
               for(int j = i+1; j < len; j++) {
                   NodeClass ncTwo = ncList.get(j);
                   if(ncOne.isCompatibleTo(ncTwo)) {
                       Node nOne = nodes.get(i);
                       Node nTwo = nodes.get(j);
                       nOne.createRelationshipTo(nTwo,RelType.LINK);
                   }
               }
            }

            tx.success();
        }
        finally
        {
            tx.finish();
        }
    }


    public String getInputPath() {
        return inputPath;
    }

    public String getGraphdbPath() {
        return graphdbPath;
    }

    public HashMap<String,Integer> getGraphPathMap() {
        return graphPathMap;
    }

    public ArrayList<String> getGraphPathList() {
        return graphPathList;
    }

    public BufferedReader getReader() {
        return reader;
    }

    public int getNumOfTests() {
        return numOfTests;
    }

    private static void registerShutdownHook( final GraphDatabaseService graphDb )
    {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook( new Thread()
        {
            @Override
            public void run()
            {
                graphDb.shutdown();
            }
        } );
    }




}
