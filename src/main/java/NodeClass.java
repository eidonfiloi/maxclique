/**
 * Created with IntelliJ IDEA.
 * User: eidonfiloi
 * Date: 7/24/13
 * Time: 8:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class NodeClass {

    private String voteFor;

    private String voteAgainst;

    public NodeClass(String voteFor, String voteAgainst) {
        this.voteFor = voteFor;
        this.voteAgainst = voteAgainst;
    }

    public boolean isCompatibleTo(NodeClass nc) {
        String vf = nc.getVoteFor();
        String va = nc.getVoteAgainst();

        if(this.voteFor.equals(va) || this.voteAgainst.equals(vf)) {
            return false;
        }

        return true;

    }

    public String getVoteFor() {
        return voteFor;
    }

    public String getVoteAgainst() {
        return voteAgainst;
    }


}
